#include <windows.h>
#include <jobapi.h>
#include <winnt.h>
#include "beacon.h"

#define JobObjectNetRateControlInformation 32
#define JOB_OBJECT_NET_RATE_CONTROL_ENABLE 1
#define JOB_OBJECT_NET_RATE_CONTROL_MAX_BANDWIDTH 2

typedef DWORD JOB_OBJECT_NET_RATE_CONTROL_FLAGS;

#define OPTION_SET_NETWORK 1
#define OPTION_SET_CPU 2
#define OPTION_SET_MEMORY 3
#define OPTION_ADD_TO_JOB 4
#define OPTION_CREATE_JOB 5
#define OPTION_TERMINATE_JOB 6

BOOL SetJobObjectNetworkMax(char *strJobName, int max_bytes_bandwidth);
BOOL SetJobObjectCpuMax(char *strJobName, int cpu_rate);
BOOL SetJobObjectMemoryMax(char *strJobName, int max_memory);
BOOL AddProcessToJob(char *strJobName, int pid);
BOOL CreateJob(char *strJobName);
BOOL TerminateJob(char *strJobName);
// BOOL DuplicateForeignJobHandle(int pid);

typedef struct JOBOBJECT_NET_RATE_CONTROL_INFORMATION
{
    DWORD64 MaxBandwidth;
    JOB_OBJECT_NET_RATE_CONTROL_FLAGS ControlFlags;
    BYTE DscpTag;
} JOBOBJECT_NET_RATE_CONTROL_INFORMATION;

/*
    Functions:
        OpenJobObjectA
        CreateJobObjectA
        SetInformationJobObject
        OpenProcess
        AssignProcessToJobObject
        CloseHandle
        TerminateJobObject
        QueryInformationJobObject

*/

DECLSPEC_IMPORT HANDLE WINAPI KERNEL32$OpenJobObjectA(DWORD, BOOL, LPCSTR);
DECLSPEC_IMPORT HANDLE WINAPI KERNEL32$CreateJobObjectA(LPSECURITY_ATTRIBUTES, char *);
DECLSPEC_IMPORT HANDLE WINAPI KERNEL32$OpenProcess(DWORD, BOOL, DWORD);

DECLSPEC_IMPORT DWORD WINAPI KERNEL32$GetLastError();

DECLSPEC_IMPORT BOOL WINAPI KERNEL32$SetInformationJobObject(HANDLE, JOBOBJECTINFOCLASS, LPVOID, DWORD);
DECLSPEC_IMPORT BOOL WINAPI KERNEL32$AssignProcessToJobObject(HANDLE, HANDLE);
DECLSPEC_IMPORT BOOL WINAPI KERNEL32$TerminateJobObject(HANDLE, UINT);
DECLSPEC_IMPORT BOOL WINAPI KERNEL32$CloseHandle(HANDLE);
DECLSPEC_IMPORT BOOL WINAPI KERNEL32$IsProcessInJob(HANDLE, HANDLE, PBOOL);
void go(char *data, int len)
{
    BOOL res = FALSE;
    datap parser;
    char *strJobName;
    int cbJobName = 0;
    int operation, arg;
    // should be formatted as "izi" or "iz"
    BeaconDataParse(&parser, data, len);
    operation = BeaconDataInt(&parser);
    strJobName = BeaconDataExtract(&parser, &cbJobName);
    if (!cbJobName)
    {
        BeaconPrintf(CALLBACK_OUTPUT, "no job");
        return;
    }

    if (operation != OPTION_CREATE_JOB && operation != OPTION_TERMINATE_JOB)
    {
        arg = BeaconDataInt(&parser);
    }

    if (operation == OPTION_CREATE_JOB)
    {
        res = CreateJob(strJobName);
    }
    else if (operation == OPTION_TERMINATE_JOB)
    {
        res = TerminateJob(strJobName);
    }
    else
    { // why does this shit keep crashing if I have a switch statement with more than 4 cases?
        switch (operation)
        {
        case OPTION_SET_CPU:
            res = SetJobObjectCpuMax(strJobName, arg);
            break;
        case OPTION_SET_NETWORK:
            res = SetJobObjectNetworkMax(strJobName, arg);
            break;
        case OPTION_SET_MEMORY:
            res = SetJobObjectMemoryMax(strJobName, arg);
            break;
        case OPTION_ADD_TO_JOB:
            res = AddProcessToJob(strJobName, arg);
            break;
        default:
            BeaconPrintf(CALLBACK_OUTPUT, "bad arg %d", arg);
            break;
        }
    }

    if (res)
    {
        BeaconPrintf(CALLBACK_OUTPUT, "+");
        return;
    }
    BeaconPrintf(CALLBACK_OUTPUT, "-");
    return;
}

// Upgrade int to int64, beacon parsing only allows extraction of int
// We *could* expand to pull out an int64
BOOL SetJobObjectNetworkMax(char *strJobName, int max_bytes_bandwidth)
{
    BOOL res = FALSE;
    HANDLE hJob;
    DWORD dwReturnLen = 0;
    JOBOBJECT_NET_RATE_CONTROL_INFORMATION jnrci = {0};
    jnrci.ControlFlags = JOB_OBJECT_NET_RATE_CONTROL_ENABLE | JOB_OBJECT_NET_RATE_CONTROL_MAX_BANDWIDTH;

    hJob = KERNEL32$OpenJobObjectA(JOB_OBJECT_SET_ATTRIBUTES, FALSE, strJobName);
    if (!hJob)
    {
        BeaconPrintf(CALLBACK_OUTPUT, "OpenJobObjectA failed! %d", KERNEL32$GetLastError());
        return FALSE;
    }

    jnrci.MaxBandwidth = max_bytes_bandwidth;

    res = KERNEL32$SetInformationJobObject(hJob, JobObjectNetRateControlInformation, &jnrci, sizeof(JOBOBJECT_NET_RATE_CONTROL_INFORMATION));
    if(!res){
        BeaconPrintf(CALLBACK_OUTPUT, "SetInformationJobObject failed! %d", KERNEL32$GetLastError());
    }
    return res;
}

/*
https://docs.microsoft.com/en-us/windows/win32/api/winnt/ns-winnt-jobobject_cpu_rate_control_information

DUMMYUNIONNAME.CpuRate

Specifies the portion of processor cycles that the threads in a job object can use during each scheduling interval, as the number of cycles per 10,000 cycles. 
If the ControlFlags member specifies JOB_OBJECT_CPU_RATE_WEIGHT_BASED or JOB_OBJECT_CPU_RATE_CONTROL_MIN_MAX_RATE, this member is not used.
Set CpuRate to a percentage times 100. 
For example, to let the job use 20% of the CPU, set CpuRate to 20 times 100, or 2,000.
Do not set CpuRate to 0. 
If CpuRate is 0, KERNEL32$SetInformationJobObject returns INVALID_ARGS.

*/
BOOL SetJobObjectCpuMax(char *strJobName, int cpu_rate)
{
    DWORD dwLastError;
    BOOL res = FALSE;
    HANDLE hJob;
    DWORD dwReturnLen = 0;
    JOBOBJECT_CPU_RATE_CONTROL_INFORMATION jcrci = {0};

    hJob = KERNEL32$OpenJobObjectA(JOB_OBJECT_SET_ATTRIBUTES, FALSE, strJobName);
    if (!hJob)
    {
        BeaconPrintf(CALLBACK_OUTPUT, "OpenJobObjectA failed! %d", KERNEL32$GetLastError());
        return FALSE;
    }

    jcrci.ControlFlags = JOB_OBJECT_CPU_RATE_CONTROL_ENABLE;
    jcrci.CpuRate = cpu_rate;
    res = KERNEL32$SetInformationJobObject(hJob, JobObjectCpuRateControlInformation, &jcrci, sizeof(JOBOBJECT_CPU_RATE_CONTROL_INFORMATION));
    if(!res){
        BeaconPrintf(CALLBACK_OUTPUT, "SetInformationJobObject failed! %d", KERNEL32$GetLastError());
    }
}

BOOL SetJobObjectMemoryMax(char *strJobName, int max_memory)
{
    BOOL res = FALSE;
    HANDLE hJob;
    DWORD dwReturnLen = 0;
    JOBOBJECT_EXTENDED_LIMIT_INFORMATION jeli; // u jeli bro?
    __stosb((LPVOID)&jeli, 0, sizeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION));
    jeli.BasicLimitInformation.LimitFlags = JOB_OBJECT_LIMIT_PROCESS_MEMORY;
    jeli.ProcessMemoryLimit = max_memory;

    hJob = KERNEL32$OpenJobObjectA(JOB_OBJECT_SET_ATTRIBUTES, FALSE, strJobName);
    if (!hJob) {
        BeaconPrintf(CALLBACK_OUTPUT, "OpenJobObjectA failed! %d", KERNEL32$GetLastError());
    	return FALSE;
    }

    res = KERNEL32$SetInformationJobObject(hJob, JobObjectExtendedLimitInformation, &jeli, sizeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION));
    if(!res){
        BeaconPrintf(CALLBACK_OUTPUT, "SetInformationJobObject failed! %d", KERNEL32$GetLastError());
    }
    KERNEL32$CloseHandle(hJob);
    return res;
    /*
	JOB_OBJECT_LIMIT_PROCESS_MEMORY
	0x00000100
	Causes all processes associated with the job to limit their committed memory. When a process attempts to commit memory that would exceed the per-process limit, it fails.
	If the job object is associated with a completion port, a JOB_OBJECT_MSG_PROCESS_MEMORY_LIMIT message is sent to the completion port.
	If the job is nested, the effective memory limit is the most restrictive memory limit in the job chain.

	This limit requires use of a JOBOBJECT_EXTENDED_LIMIT_INFORMATION structure. 
	Its BasicLimitInformation member is a JOBOBJECT_BASIC_LIMIT_INFORMATION structure.
	*/
}

BOOL AddProcessToJob(char *strJobName, int pid)
{
    BOOL bIsProcInJob = FALSE;
    BOOL res;
    HANDLE hJob;
    HANDLE hProcess;

    hJob = KERNEL32$OpenJobObjectA(JOB_OBJECT_ALL_ACCESS, FALSE, strJobName);
    if (!hJob)
    {
        BeaconPrintf(CALLBACK_OUTPUT, "OpenJobObject failed! %d", KERNEL32$GetLastError());
        KERNEL32$CloseHandle(hJob);
        return FALSE;
    }

    hProcess = KERNEL32$OpenProcess(PROCESS_SET_QUOTA | PROCESS_TERMINATE, FALSE, pid);
    if (!hProcess)
    {
        BeaconPrintf(CALLBACK_OUTPUT, "OpenProcess failed! %d", KERNEL32$GetLastError());
        KERNEL32$CloseHandle(hJob);
        return FALSE;
    }

    KERNEL32$IsProcessInJob(hProcess, hJob, &bIsProcInJob);
    if (bIsProcInJob)
    {
        BeaconPrintf(CALLBACK_OUTPUT, "Already in job");
        // process in another job, can't do anything
        KERNEL32$CloseHandle(hJob);
        KERNEL32$CloseHandle(hProcess);
        return FALSE;
    }

    res = KERNEL32$AssignProcessToJobObject(hJob, hProcess);

    KERNEL32$CloseHandle(hJob);
    KERNEL32$CloseHandle(hProcess);
    return res;
}

BOOL CreateJob(char *strJobName)
{
    BeaconPrintf(CALLBACK_OUTPUT, "Creating job...");
    HANDLE hJob;
    hJob = KERNEL32$CreateJobObjectA(NULL, strJobName);
    return hJob > 0;
}

BOOL TerminateJob(char *strJobName)
{
    HANDLE hJob;
    BOOL res;
    KERNEL32$OpenJobObjectA(JOB_ALL_ACCESS, FALSE, strJobName);
    if (!hJob)
    {
        return FALSE;
    }
    
    res = KERNEL32$TerminateJobObject(hJob, 0);
    KERNEL32$CloseHandle(hJob);
    return res;
}
