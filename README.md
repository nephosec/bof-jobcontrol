### BOF - Job API

This project serves as a BOF equivalent of my project:

https://github.com/alfarom256/jobcontrol/src/

Commands:

```
<JOB> : String job name

jobify create <JOB> 
    - Creates a job with the given string name

job terminate <JOB>
    - Terminates a job, and all processes within it

jobify add <JOB> <PID>
    - Adds a process to an existing job by job name

jobify cpu <JOB> <DECIMAL_PCT>
    - Limits the percent CPU util. of ALL PROCESSES within the job

jobify net <JOB> <MAX_BYTES_BANDWITH>
    - Limits the bandwith (in bytes) for ALL PROCESSES within the job

jobify mem <JOB> <MAX_BYTES_COMMITTED>
    - Limits the committed memory of ALL PROCESSES within the job

```

Thank you to Nephosec for letting me re-release :)
